# letompouce/vscode

Docker image for Microsoft Visual Code.

## run

Mount the Docker socket to use the Docker plugin:

```shell
docker run -d \
  -v /tmp/.X11-unix:/tmp/.X11-unix \
  -v $HOME:/home/tom \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -e DISPLAY=unix$DISPLAY \
  --device /dev/dri \
  --name vscode \
  letompouce/vscode
```

## install

```shell
# create an executable
cp contrib/usr_local_bin_vscode.sh /usr/local/bin/vscode
# add a menu entry
cp contrib/vscode.desktop ${HOME}/.local/share/applications/vscode.desktop
# copy the icon
cp contrib/vscode-480.png /usr/local/share/icons/vscode-480.png
```

## tags

### Simple tags

* `a1b2c3d4`: CI build at `$CI_COMMIT_SHORT_SHA`
* `v1.0`: CI build at `$CI_COMMIT_TAG`.

### Shared tags

* `latest`: Points to latest `vX.X` release tag. Stable image that should be
  used in production.
* `head`: points to latest `push` build. May not be production-ready.
